# Warning
Character bounty feature was removed from EVE Online few months ago. Therefore project was discontinued. Bot can be still used to track certain characters in game, but there is no practical use for it.

# EVE Online character tracking bot

It is very early version of project.

Go to `source` folder and type `npm install`. Then copy `botconfig.example` and rename it to `botconfig.json`. Insert proper data and run with `node .`

Made by Provagus
