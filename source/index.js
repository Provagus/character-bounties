const botconfig = require("./botconfig.json");
const Discord = require("discord.js");
const bot = new Discord.Client({disableEveryone: true});
var WebSocket = require('ws');
var webSocketBridge = new WebSocket('wss://zkillboard.com/websocket/');
const got = require('got');
var relayChannel;
const version = "v1.0";
var botStatus = false;

bot.on("ready", async () => {
    console.log(`${bot.user.username} is online!`);
    bot.user.setActivity(botconfig.botActivty, {type: botconfig.activityType});
    relayChannel = await bot.channels.fetch(botconfig.relay).catch(ex => console.error(ex));
    botStatus = true;
});

async function checkCharacter(id){
    var response = await got(`https://esi.evetech.net/latest/characters/${id}/`);
    response = JSON.parse(response.body);
    return response.name;
}

function listenToZkill(){
    webSocketBridge.send('{"action":"sub","channel":"killstream"}');
    webSocketBridge.onmessage = async (action) => {
        if(botStatus){
            var data = JSON.parse(action['data']);
            var trackedCharacterID;
            var tracked = false;
            if(botconfig.wanted.includes(data.victim.character_id)){
                tracked = true;
                trackedCharacterID = data.victim.character_id;
            }
            else{
                for(let element of data.attackers){
                    if(botconfig.wanted.includes(element.character_id)){
                        tracked = true;
                        trackedCharacterID = element.character_id;
                        break;
                    }
                }
            }
            if(tracked){
                var response = await got(`https://esi.evetech.net/latest/universe/systems/${data.solar_system_id}/`);
                response = JSON.parse(response.body);
                var botem = new Discord.MessageEmbed();
                botem.setTitle("New activity");
                botem.addField("Name", `${await checkCharacter(trackedCharacterID)}`);
                botem.addField("System", `${response.name}`);
                botem.addField("Killmail link", `[zKill](${data.zkb.url})`);
                botem.setFooter(`Bounty bot ${version} by Provagus`);
                botem.setThumbnail(`https://images.evetech.net/characters/${trackedCharacterID}/portrait?size=64`);
                relayChannel.send(botem);
            }
        }
    };
}

webSocketBridge.onopen = function(){
    console.log("Connected");
    listenToZkill();
};

webSocketBridge.onclose = function(){
    console.log("Closed");
};

bot.login(botconfig.token);